package goosegame.core;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


//curl -d "add player pippo" -H "Content-Type: application/text" -X POST http://127.0.0.1:8080/



@RestController
public class GameController {

	private static LinkedHashMap<String,Integer> mapPlayers=new LinkedHashMap<String,Integer>();
	private static HashMap<Integer, String> keyPosMap=new HashMap<Integer, String>();


	

	@RequestMapping(value="/", method = RequestMethod.POST)
	public ResponseEntity<?> playGame(@RequestBody String postPayload) 
	{
		initKeyPosMap();
		ResponseEntity<?> esito ="";   
		String[] tags=postPayload.split("\\s+");

		if(tags[0].equalsIgnoreCase("add"))
		{	
			esito =ResponseEntity.status(HttpStatus.OK).body(executeAdd(tags)); 
		}
		else if(tags[0].equalsIgnoreCase("move"))
		{
			esito =ResponseEntity.status(HttpStatus.OK).body(executeMove(tags));
		}	
		else
			esito =ResponseEntity.status(HttpStatus.OK).body("unknown action");   
		return esito;
	}


	private void initKeyPosMap() {
		keyPosMap.put(6, "The Bridge");
		keyPosMap.put(5, "The Goose");
		keyPosMap.put(9, "The Goose");
		keyPosMap.put(14, "The Goose");
		keyPosMap.put(18, "The Goose");
		keyPosMap.put(23, "The Goose");
		keyPosMap.put(27, "The Goose");

	}


	private String executeMove(String[] tags) {
		String response="";
		String name=tags[1];
		
		int oldpos=mapPlayers.get(name);		
		

		int newpos;
		int firstNum;
		int secondNum;

		//Dices launch
		if(tags.length<3 )  
		{ 
			firstNum = 1 + Double.valueOf(Math.random()*(6)).intValue();
			secondNum = 1 + Double.valueOf(Math.random()*(6)).intValue();
			

		}else
		{	
			firstNum=Integer.valueOf(tags[2].substring(0, tags[2].lastIndexOf(",")).trim());
			secondNum=Integer.valueOf(tags[3].trim());

		}
	
		newpos=oldpos+firstNum+secondNum;
		

		String strPos;
		if (oldpos==0)
			strPos="Start";
		else
			strPos=Integer.toString(oldpos);	

		int destpos=0;
		if(newpos>63)
			destpos=63;
		else
			destpos=newpos;

		String partialResp=String.format("%s rolls %s, %s. %s moves from %s to %s", name,Integer.toString(firstNum),
				Integer.toString(secondNum),name,strPos,
				(keyPosMap.get(destpos)!=null && !((String)keyPosMap.get(destpos)).equalsIgnoreCase("The Goose"))? keyPosMap.get(destpos):Integer.toString(destpos));
		
		response=checkNewPos(name,newpos,Integer.toString(firstNum),Integer.toString(secondNum),oldpos,partialResp);
		
		return response;
	}


	private String checkNewPos(String name,int newpos,String firstNum,String secondNum,int strPos,String resp) {
		
		if (newpos==6)
		{
			resp=resp+String.format(". %s  jumps to 12.",name);
			newpos=12;
		}
		else if (newpos==63)
		{
			resp=resp+String.format(". %s Wins!!", name);
		}
		else if (newpos>63)
		{
			newpos=63-(newpos-63);
			resp=resp+String.format(". %s bounces! %s return to %s",name,name,Integer.toString(newpos));
		}
		else if(keyPosMap.get(newpos)!=null && ((String)keyPosMap.get(newpos)).equalsIgnoreCase("The Goose"))
		{
			int intermadiateNewpos=newpos;
			newpos=intermadiateNewpos+Integer.valueOf(firstNum)+Integer.valueOf(secondNum);
			resp=resp+String.format(", The Goose. %s moves again and goes to %s",name,Integer.toString(newpos))+insertMapPlayers(strPos,newpos,name);
			strPos=newpos;
			resp=checkNewPos(name,newpos,firstNum,secondNum,strPos,resp);	

		}
		resp=resp+insertMapPlayers(strPos,newpos,name);
	
		return resp;
	}





	private String insertMapPlayers(int strPos,int newpos, String firstPlayer) {
		String resp="";

		String secondPlayer=(String) getKeyFromValue(mapPlayers, new Integer(newpos));

		if(secondPlayer!=null && !secondPlayer.equalsIgnoreCase(firstPlayer))
		{
			mapPlayers.put(secondPlayer, Integer.valueOf(strPos));	
			mapPlayers.put(firstPlayer, newpos);
			resp=" On "+newpos+" there is "+secondPlayer+", who returns to "+strPos;
		}	
		else
		{
			mapPlayers.put(firstPlayer,newpos);
		}
			return resp;
	}



	private String executeAdd(String[] tags) {	
		if(tags.length<3 || tags[2]==null || tags[2].isEmpty())  
			return "missing player name";
		else if(mapPlayers.get(tags[2])==null)
		{
			mapPlayers.put(tags[2],new Integer(0));
			StringBuilder str=new StringBuilder();

			int i=0;
			for( String key:mapPlayers.keySet())
			{
				if(i==0)	
					str.append(key);
				else
					str.append(", "+key);
				i++;
			}

			return "Players:"+str.toString();
		}
		else
			return tags[2]+": already existing player";
	}

	
	private static Object getKeyFromValue(LinkedHashMap<String,Integer> hm, Object value) {
        for (Object o : hm.keySet()) {
          if (hm.get(o).equals(value)) {
            return o;
          }
        }
        return null;
      }


}
